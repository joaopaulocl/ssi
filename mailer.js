var nodemailer = require('nodemailer');

// create reusable transporter object using the default SMTP transport
var transporter = nodemailer.createTransport('smtps://rexlabufsc%40gmail.com:ufsc1234@smtp.gmail.com');

// setup e-mail data with unicode symbols
var mailOptions = {
    from: '"RELLE notification" <rexlabufsc@gmail.com>', 
    to: 'jp.limapk@gmail.com', 
    //to: 'jp.limapk@gmail.com,josepedrosimao@gmail.com,josiel-pereira18@hotmail.com,lucas.mellos@grad.ufsc.br', 
    subject: 'Notification', // Subject line
    text: 'Hello', // plaintext body
    html: '' // html body
};


module.exports = function(subject, message, attachment) {

	mailOptions.subject = subject;
	mailOptions.text = message;
        if(typeof(attachment) == 'object'){
            mailOptions.attachments = [attachment];
        }
	transporter.sendMail(mailOptions, function(error, info){
	    if(error){
		return console.log(error);
	    }
	    console.log('Message sent: ' + info.response);
	});

};